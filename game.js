// the game itself
var game;
// the spinning wheel
var wheel_thumbs; 
// can the wheel spin?
var canSpin;
// slices (prizes) placed in the wheel
var slices = 6;
// prize names, starting from 12 o'clock going clockwise
var slicePrizes = ["Helipopper", "Simon Sees", "Turtle Wax", "When Pigs Fly", "Pickle", "Vine Time"];
// the prize you are about to win
var prize;
// text field where to show the prize
var prizeText;


window.onload = function() {	
     // creation of a 458x488 game
	game = new Phaser.Game(491, 491, Phaser.AUTO, "");
     // adding "PlayGame" state
     game.state.add("PlayGame",playGame);
     // launching "PlayGame" state
     game.state.start("PlayGame");
}

// PLAYGAME STATE


	
var playGame = function(game){};

playGame.prototype = {
     // function to be executed once the state preloads
     preload: function(){
          // preloading graphic assets
          
          game.load.image('wheel_thumb1', "1.png");
          game.load.image("wheel_thumb2", "2.png");
          game.load.image("wheel_thumb3", "3.png");
          game.load.image("wheel_thumb4", "4.png");
          game.load.image("wheel_thumb5", "5.png");
          game.load.image("wheel_thumb6", "6.png");
		game.load.image("pin", "spin2.png");     

          game.load.image("background", "background.png");
          game.load.image("playbutton", "play-button2.png");
    
     },
     // funtion to be executed when the state is created
  	create: function(){
          // giving some color to background
  		//game.stage.backgroundColor = "#FFFFFF;
          sprite = game.add.tileSprite(0, 0, 491, 491, "background");           
          


          // adding the wheel in the middle of the canvas (wheel_thumbs1 - 6)
  		wheel_thumbs = game.add.sprite(245,254);
          wheel_thumbs.anchor.set(0.5);

          var wheel_thumb1 = wheel_thumbs.addChild(game.add.sprite(0,-48,'wheel_thumb1'));
          wheel_thumb1.anchor.set(.5, 1);
          wheel_thumb1.angle = 0;
          
          var wheel_thumb2 = wheel_thumbs.addChild(game.add.sprite(30,-25, 'wheel_thumb2'));
          wheel_thumb2.anchor.set(.5, 1);
          wheel_thumb2.angle = 60;
          
          var wheel_thumb3 = wheel_thumbs.addChild(game.add.sprite(18,10, 'wheel_thumb3'));
          wheel_thumb3.anchor.set(.5, 1);
          wheel_thumb3.angle = 120;
          
          var wheel_thumb4 = wheel_thumbs.addChild(game.add.sprite(0,25, 'wheel_thumb4'));
          wheel_thumb4.anchor.set(.5, 1);
          wheel_thumb4.angle = 180;

          var wheel_thumb5 = wheel_thumbs.addChild(game.add.sprite(-30,15, 'wheel_thumb5'));
          wheel_thumb5.anchor.set(.5, 1);    
          wheel_thumb5.angle = -120;

          var wheel_thumb6 = wheel_thumbs.addChild(game.add.sprite(-36,-28, 'wheel_thumb6'));
          wheel_thumb6.anchor.set(.5, 1);
          wheel_thumb6.angle = -60;


          // adding the pin in the middle of the canvas
          var pin = game.add.sprite(245,245,"pin");
          // setting pin registration point in its center
          pin.anchor.set(0.5);
          pin.pivot.setTo(.5, 1);

          //playbutton as a group

          playbuttongroup = game.add.sprite();


          var playbutton = playbuttongroup.addChild(game.add.sprite(10,210,"playbutton"));

          // bring to top function

          playbutton = game.world.bringToTop(playbuttongroup);


          // adding the text field
          prizeText = game.add.text(game.world.centerX, 480, "");
          // setting text field registration point in its center
          prizeText.anchor.set(0.5);
          // aligning the text to center
          prizeText.align = "center";
          // the game has just started = we can spin the wheel
          canSpin = true;
          // waiting for your input, then calling "spin" function
          game.input.onDown.add(this.spin, this);		
	},
     // function to spin the wheel
     spin(){
          // can we spin the wheel?
          if(canSpin){  
               // resetting text field
               prizeText.text = "";
               // the wheel will spin round from 2 to 4 times. This is just coreography
               var rounds = game.rnd.between(2, 4);
               // then will rotate by a random number from 0 to 360 degrees. This is the actual spin
               var degrees = game.rnd.between(0, 360);
               // before the wheel ends spinning, we already know the prize according to "degrees" rotation and the number of slices
               prize = slices - 1 - Math.floor(degrees / (360 / slices));
               // now the wheel cannot spin because it's already spinning
               canSpin = false;
               // animation tweeen for the spin: duration 3s, will rotate by (360 * rounds + degrees) degrees
               // the quadratic easing will simulate friction
               var spinTween = game.add.tween(wheel_thumbs).to({
                    angle: 360 * rounds + degrees
               }, 3000, Phaser.Easing.Quadratic.In, true);
               // once the tween is completed, call winPrize function
               spinTween.onComplete.add(this.winPrize, this);
          }
     },
     // function to assign the prize
     winPrize(){
          // now we can spin the wheel again
          canSpin = true;
          // writing the prize you just won
          prizeText.text = slicePrizes[prize];
     }
}